import { Quote } from './../../data/quote.interface';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ViewController
} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-quote',
  templateUrl: 'quote.html'
})
export class QuotePage {
  person: string;
  text: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private viewController: ViewController
  ) {}

  onClose(unfavoriteQuote = false) {
    this.viewController.dismiss(unfavoriteQuote);
  }

  ionViewDidLoad() {
    this.person = this.navParams.get('person');
    this.text = this.navParams.get('text');
  }
}
