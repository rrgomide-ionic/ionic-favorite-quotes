import { QuotesService } from './../../services/quotes.service';
import { Quote } from './../../data/quote.interface';
import { Component, OnInit } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  AlertController
} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-quotes',
  templateUrl: 'quotes.html'
})
export class QuotesPage implements OnInit {
  quoteGroup: { category: string; quotes: Quote[]; icon: string };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private quotesService: QuotesService
  ) {}

  /**
   * Nessa abordagem, devemos utilizar o operador "elvis" (?)
   * no template para que o template só seja renderizado quando
   * this.quoteGroup estiver devidamente carregado
   */
  // ionViewDidLoad() {
  //   this.quoteGroup = this.navParams.data;
  // }

  /**
   * Aqui, não precisamos de (?) no template,
   * pois ngOnInit só é invocado após o carregamento dos dados
   */
  ngOnInit() {
    this.quoteGroup = this.navParams.data;
  }

  onRemoveFromFavorites(quoteToRemove: Quote) {
    this.quotesService.removeQuoteFromFavorites(quoteToRemove);
  }

  onAddToFavorites(selectedQuote: Quote) {
    const alert = this.alertCtrl.create({
      title: 'Adicionar citação',
      subTitle: 'Tem certeza?',
      message:
        'Tem certeza de que deseja adicionar esta citação aos Favoritos?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancelou');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.quotesService.addQuoteToFavorites(selectedQuote);
            console.log('Adicionou');
          }
        }
      ]
    });
    alert.present();
  }

  isFavorite(quote) {
    return this.quotesService.isFavorite(quote);
  }
}
