import { IonicPage } from 'ionic-angular';
import { Component } from '@angular/core';

@IonicPage()
@Component({
  selector: 'page-tabs',
  template: `
    <!--
      Comentário Raphael Gomide
      selectedIndex indica qual aba vai aparecer primeiro
    -->
    <ion-tabs selectedIndex="1">
      <ion-tab [root]="favoritesPage" tabTitle="Favoritos" tabIcon="star"></ion-tab>
      <ion-tab [root]="libraryPage" tabTitle="Repositório" tabIcon="book"></ion-tab>
    </ion-tabs>
  `
})
export class TabsPage {
  favoritesPage = 'FavoritesPage';
  libraryPage = 'LibraryPage';
}
