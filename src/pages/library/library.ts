import { Quote } from './../../data/quote.interface';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import quotesMock from '../../data/quotes.mock';

@IonicPage()
@Component({
  selector: 'page-library',
  templateUrl: 'library.html'
})
export class LibraryPage implements OnInit {
  /**
   * Coleção de quotes a ser percorrida
   */
  quoteCollection: { category: string; quotes: Quote[]; icon: string }[];

  /**
   * Indicador da página a ser
   * acionada no click de algum
   * item da lista
   */
  quotesPage = 'QuotesPage';

  ngOnInit() {
    this.quoteCollection = quotesMock;
  }
}
