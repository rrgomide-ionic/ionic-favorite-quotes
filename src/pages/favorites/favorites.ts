import { QuotesService } from './../../services/quotes.service';
import { Quote } from './../../data/quote.interface';
import { Component } from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController
} from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html'
})
export class FavoritesPage {
  favoriteQuotes: Quote[];

  constructor(
    private quotesService: QuotesService,
    private modalController: ModalController
  ) {}

  ionViewDidEnter() {
    this.reload();
  }

  reload() {
    this.favoriteQuotes = this.quotesService.getFavoriteQuotes();
  }

  onViewQuote(quote: Quote) {
    const modal = this.modalController.create('QuotePage', quote);
    modal.present();

    modal.onDidDismiss((removeQuote: boolean) => {
      if (removeQuote) {
        this.onRemoveFromFavorites(quote);
      }
    });
  }

  onRemoveFromFavorites(quote: Quote) {
    this.quotesService.removeQuoteFromFavorites(quote);

    /**
     * Recarregando os dados
     */
    this.reload();
  }
}
