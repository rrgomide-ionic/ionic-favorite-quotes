import { Quote } from './../data/quote.interface';

export class QuotesService {
  private _favoriteQuotes: Quote[] = [];

  addQuoteToFavorites(quote: Quote) {
    this._favoriteQuotes.push(quote);
    console.log(this._favoriteQuotes);
  }

  removeQuoteFromFavorites(quote: Quote) {
    this._favoriteQuotes = this._favoriteQuotes.filter(
      item => item.id !== quote.id
    );
  }

  getFavoriteQuotes() {
    /**
     * Poderia ser...
     */
    //return this._favoriteQuotes.slice();

    /**
     * Retornamos uma cópia do objeto, garantindo
     * a imutabilidade
     */
    return Object.assign([], this._favoriteQuotes);
  }

  isFavorite(quote) {
    return this._favoriteQuotes.some(item => item.id === quote.id);
  }
}
